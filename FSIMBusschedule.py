#!/usr/bin/env python
# -*- coding: utf8 -*-
from datetime import datetime, timedelta
import json
import threading
from flask import Flask, render_template
import requests
from bs4 import BeautifulSoup
import sys
from werkzeug.contrib.cache import SimpleCache
import os
from SocketServer import UDPServer, BaseRequestHandler
import logging

logging.getLogger('werkzeug').setLevel(logging.ERROR)
c = SimpleCache()
app = Flask(__name__)
door_state = None

# remove these strings from the result
remove = ['Regensburg,', 'Regensburg', 'Rgbg.', 'Burgw.']

# list of destinations indicating the bus is driving into town
rwslist = ['HBF/Albertstraße', 'Wernerwerkstraße', 'Wernerwerkstr.', 'Regensburg Hbf', 'Roter-Brach-Weg', 'Dachauplatz',
           'Schwabenstr.', 'Schwabenstraße', 'Arnulfsplatz', 'H.-Höcherl-Straße']

# url to load the schedule from
#scheduleurl = 'http://bayern-fahrplan.de/xhr_departures_monitor?limit=40&zope_command=dm_next&mode_type=&hide_map=&time_or_date_changed=False&currentViewName=DeparturesView&computationType=sequence&onload_marker_data=&dwellTime=&ptOptionsActive=1&itOptionsActive=1&stateless=1&odvWeightingMacro=beg&anySigWhenPerfectNoOtherMatches=1&coordOutputFormat=WGS84%5BDD.DDDDD%5D&itdLPxx_mode=&itdLPxx_timeOffset=10&dm_fullscreen_delay_mills=0&itdLPxx_reqparams_HE9GFQ=inclMOT_13%3D1%26inclMOT_10%3D1%26inclMOT_11%3D1%26inclMOT_16%3D1%26inclMOT_14%3D1%26inclMOT_15%3D1%26timeOffset%3D10%26name_dm%3DRegensburg%252C%2BUniversit%25C3%25A4t%26nameInfo_dm%3D4014080%26itdDateTimeDepArr%3Ddep%26inclMOT_1%3D1%26inclMOT_2%3D1%26inclMOT_3%3D1%26inclMOT_4%3D1%26inclMOT_5%3D1%26inclMOT_6%3D1%26inclMOT_7%3D1%26inclMOT_8%3D1%26inclMOT_9%3D1%26includedMeans%3Dcheckbox%26anyObjFilter_dm%3D0%26type_dm%3Dany%26mode_type%3D%26is_fs%3D1%26mode%3Ddirect%26coordOutputFormat%3DWGS84%255BDD.DDDDD%255D%26is_xhr%3DTrue&last_departure_time=22%3A30&last_departure_date=09.05.2016&itdDateTimeDepArr=dep&name_dm=1&nameInfo_dm=4014080&type_dm=any&line=rvv%3A11004%3AP%3AH%3A4fd&line=rvv%3A11006%3AP%3AH%3A4fd&line=rvv%3A11006%3AP%3AR%3A4fd&line=rvv%3A11011%3AP%3AH%3A4fd&line=rvv%3A11011%3AP%3AR%3A4fd&line=gfn%3A12019%3A+%3AH%3A15s&line=gfn%3A12019%3A+%3AR%3A15s&line=rvv%3A11002%3AP%3AH%3A4fd&line=rvv%3A11002%3AP%3AR%3A4fd&line=rvv%3A11061%3AP%3AR%3A4fd&line=rvv%3A11062%3AP%3AH%3A4fd&line=rvv%3A11064%3AP%3AH%3A4fd&line=rvv%3A11066%3AP%3AR%3A4fd&includedMeans=1&inclMOT_3=1&inclMOT_7=1&inclMOT_8=1&inclMOT_11=1&inclMOT_14=1&inclMOT_16=1&inclMOT_13=1&inclMOT_2=1&inclMOT_5=1&inclMOT_9=1&inclMOT_15=1&inclMOT_1=1&inclMOT_4=1&inclMOT_6=1&inclMOT_10=1'
#scheduleurl = 'http://www.bayern-fahrplan.de/xhr_departures_monitor?limit=40&zope_command=dm_next&nameInfo_dm=4014080&itdDateDayMonthYear=12.12.2016&itdTime=12%3A00'
scheduleurl = 'http://www.bayern-fahrplan.de/xhr_departures_monitor?limit=40&zope_command=dm_next&nameInfo_dm=4014080'

class Bus:
    def __init__(self, time, route, direction, stop=0):

        for r in remove:
            direction = direction.replace(r, '').strip()

        # fix layout issue
        direction = direction.replace('Hermann-', 'H.-')

        # replacement not needed anymore - wait for a while with removal to be sure
        if route == '2A/B':
            route = '2'

        self.rws = (direction.encode('utf-8') in rwslist)
        self.time = time
        self.route = route
        self.direction = direction
        self.stop = stop

    def __str__(self):
        return '(%s: [%s] %s)' % (self.time, self.route, self.direction)


    def getRemainingTime(self):
        return self.time - datetime.now()


    def getRemainingTimeHM(self):
        hours, remainder = divmod(self.getRemainingTime().seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        return [hours, minutes + 1]


    def getCssClass(self):
        diff = (self.time - datetime.now()).seconds
        if diff > 600:
            return "text-success"
        elif diff > 300:
            return "text-warning"
        else:
            return "text-danger"


@app.route('/')
def getIndex():
    return render_template('index.html')


@app.route('/table')
def gettable():
    try:
        lst = c.get('list')
        i = 0
        while lst is None and i < 2:
            i += 1
            print 'reloading schedule - #%i' % i
            lst = sorted(getdepartures(), key=lambda bus: bus.time)
            c.set('list', lst, timeout=60 * 3)

        if i == 0:
            print "using cache"

        lstf = filter(lambda v: v.time > datetime.now() + timedelta(seconds=30), lst)

        weather = c.get('weather')
        if weather is None:
            weather = getweather()['forecast']['simpleforecast']['forecastday']
            c.set('weather', weather, timeout=60 * 15)

        if lstf is not None:
            # c.set('list', lstf, timeout=13)
            return render_template('table.html', list=lstf, weather=weather)
        else:
            return render_template('error.html', error="Kein Fahrplan")

    except requests.ConnectionError:
        return render_template('error.html', error="Keine Verbindung")

    except Exception as e:
        return render_template('error.html', error=e)
        # return render_template('error.html', error=sys.exc_info()[0])


# load the weather data for Regensburg
# the api key can be found in the url
def getweather():
    try:
        f = requests.get('http://api.wunderground.com/api/8bd799fab25d5b2e/forecast/lang:DL/q/Regensburg.json')
        parsed_json = json.loads(f.text)
        return parsed_json
    except Exception:
        return None


# load the departure list and parse it
def getdepartures():
    try:
        r = requests.get(scheduleurl)
        data = r.text
        soup = BeautifulSoup(data, "lxml")
        departures = []
        resulttables = soup.find_all('table', class_='trip')

        for i, trip in enumerate(resulttables):
            try:
                cells = trip.tbody.tr.find_all('td')

                linkdata = json.loads(cells[1].a['data-stopseq_linkdata'])
                ptime = datetime.strptime("%s %s" % (linkdata['date'], linkdata['time']), '%Y%m%d %H:%M')

                # add delay to time
                try:
                    dtime = int(cells[0].span.span.text.strip())
                    ptime += timedelta(minutes=dtime)
                except:
                    pass

                tripobject = Bus(time=ptime, route=cells[1].a.span.text.strip(), direction=cells[2].text.strip(),
                                 stop=cells[3].text.strip())

                departures.append(tripobject)
            except Exception as e:
                print "Unexpected error in trip: " % e

        return departures
    except Exception as e:
        print "Unexpected error: %s" % e
        raise
        # return None


class UDPBroadcastHandler(BaseRequestHandler):
    def handle(self):
        global door_state
        print "message:", self.request[0]
        # print "from:", self.client_address
        if self.request[0] == 'DOOR_OPEN' and self.request[0] != door_state:
            door_state = self.request[0]
            os.system("tvservice -p")
        elif self.request[0] == 'DOOR_CLOSED' and self.request[0] != door_state:
            door_state = self.request[0]
            os.system("tvservice -o")


class UDPBroadcastReceiver(threading.Thread):
    def run(self):
        try:
            addr = ("", 5555)
            print "UDP server listening on", addr
            server = UDPServer(addr, UDPBroadcastHandler)
            server.serve_forever()
        except:
            print "Couldn't start UDP server"


if __name__ == '__main__':
    udpserver = UDPBroadcastReceiver()
    udpserver.start()
    app.debug = False
    app.run(use_reloader=True)
